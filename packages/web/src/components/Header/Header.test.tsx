import React from 'react';
import { render, screen, cleanup, fireEvent } from '@testing-library/react';
import Header from './Header';

afterAll(() => {
  cleanup();
});

describe('Unit Testing for Header component', () => {
  test('Expected render a correct text for 300 news', () => {
    render(<Header totalNews={300} search={() => {}} />);
    let headerElement = screen.getByTestId('app-header-test');
    expect(headerElement).toBeInTheDocument();
    expect(headerElement).toHaveTextContent(
      'HN FeddWe have more than 300 news'
    );
  });

  test('Expected search bar works correctlly', () => {
    render(
      <Header
        totalNews={300}
        search={(e: string) => {
          return expect(e).toBe('Jhon Keens Adams');
        }}
      />
    );
    let searchBar: any = screen.getByTestId('app-header-test-search-bar');
    expect(searchBar).toBeInTheDocument();
    fireEvent.change(searchBar, { target: { value: 'Jhon K' } });
    expect(searchBar.value).toBe('Jhon K');
    fireEvent.change(searchBar, { target: { value: 'Jhon Keens Adams' } });
    fireEvent.keyDown(searchBar, { key: 'Enter' });
  });
});
