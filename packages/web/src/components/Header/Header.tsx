import React, { Component } from 'react';
import './Header.scss';
class Header extends Component<
  {
    totalNews: number;
    search: Function;
  },
  any
> {
  state = {
    searchText: '',
  };
  _handleKeyDown(e: any) {
    if (e.key === 'Enter') {
      this.props.search(this.state.searchText);
    }
  }
  render(): JSX.Element {
    return (
      <div data-testid="app-header-test" className='Header'>
        <div className='container'>
          <h1>HN Fedd</h1>
          <h3>We have more than {this.props.totalNews} news</h3>
          <div className='search'>
            <input
              data-testid="app-header-test-search-bar"
              value={this.state.searchText}
              onChange={(e) => this.setState({ searchText: e.target.value })}
              onKeyDown={this._handleKeyDown.bind(this)}
              type='search'
              placeholder='Search'
            />
            <i className='fas fa-search'></i>
          </div>
        </div>
      </div>
    );
  }
}
export default Header;
