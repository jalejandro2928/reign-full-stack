import React, { Component } from 'react';
import './News.scss';
class New extends Component<{ news: any; deleteNews: Function }, any> {
  renderTitle(news: any) {
    return news?.story_title || news?.title || 'No asigned';
  }
  renderUrl(news: any) {
    return news?.story_url || news?.url || '/';
  }
  formatDate(date: string): string {
    let moment = new Date(date);
    let DD = formatNumber(moment.getDate());
    let MM = formatNumber(moment.getMonth() + 1);
    let YYYYY = formatNumber(moment.getFullYear());
    let hh = formatNumber(moment.getUTCHours());
    let mm = formatNumber(moment.getUTCMinutes());
    return `${hh}:${mm} ${DD}/${MM}/${YYYYY}`;
    function formatNumber(n: number) {
      return n < 10 ? '0' + n : n;
    }
  }
  onDeleteNews(event: any) {
    event.preventDefault();
    this.props.deleteNews(this.props.news._id);
  }
  render() {
    return (
      <div data-testid={'news_' + this.props.news._id} className='News'>
        <a
          href={this.renderUrl(this.props?.news)}
          target='_blank'
          rel='noreferrer'
        >
          <div className='title'>
            {this.renderTitle(this.props?.news)}
            <span className='author'>-{this.props.news.author}-</span>
          </div>
          <div className='date'>
            {this.formatDate(this.props.news.created_at)}
          </div>
          <button onClick={(e) => this.onDeleteNews(e)} className='deleteBtn'>
            <i className='far fa-trash-alt'></i>
          </button>
        </a>
      </div>
    );
  }
}
export default New;
