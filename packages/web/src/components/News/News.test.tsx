import React from 'react';
import { render, screen, cleanup, fireEvent } from '@testing-library/react';
import News from './News';

afterEach(() => {
  cleanup();
});

let news = {
  _id: 'adsasarefaerfad',
  author: 'Jose Alejandro Concepcion',
  story_title: 'Tell me about NodeJs + Typescript',
  story_url: 'https://josealejandro2928.github.io/portfolio/',
  created_at: '2021-06-03T15:42:40.000Z',
};

describe('Unit Testing for News component', () => {
  test('Expected render a correct html data', () => {
    render(<News news={news} deleteNews={(id: number) => {}} />);
    let newsElement = screen.getByTestId(`news_${news._id}`);
    expect(newsElement).toBeInTheDocument();
    let authorEl = newsElement.querySelector('.author');
    expect(authorEl).toBeInTheDocument();
    expect(authorEl?.innerHTML).toMatch(news.author);
  });

  test('Expected news render the time format correctly', () => {
    render(<News news={news} deleteNews={(id: number) => {}} />);
    let newsElement = screen.getByTestId(`news_${news._id}`);
    expect(newsElement).toBeInTheDocument();
    let dateEl = newsElement.querySelector('.date');
    expect(dateEl?.innerHTML).toBe('15:42 03/06/2021');
  });

  test('Expected passing the correct id when deleted button is clicked', () => {
    render(
      <News
        news={news}
        deleteNews={(id: number) => {
          return expect(id).toBe(news._id);
        }}
      />
    );
    let newsElement = screen.getByTestId(`news_${news._id}`);
    let deleteBtn: any = newsElement.querySelector('.deleteBtn');
    expect(deleteBtn).toBeInTheDocument();
    fireEvent.click(deleteBtn);
  });
});
