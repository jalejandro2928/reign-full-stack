import React, { Component } from 'react';
import Header from '../../components/Header/Header';
import './App.scss';
import axios from 'axios';
import News from '../../components/News/News';

type AppState = {
  allNews: any[];
  totalNews: number;
  searchText: string;
  pagination: {
    limit: number;
    offset: number;
    total: number;
    count: number;
  };
};

const SERVER_URL = 'http://localhost:3333';

class App extends Component<any, AppState> {
  state: AppState = {
    allNews: [],
    totalNews: 0,
    searchText: '',
    pagination: {
      limit: 10,
      offset: 0,
      total: 0,
      count: 0,
    },
  };

  async componentDidMount() {
    try {
      await this.refreshData();
    } catch (error) {
      // console.log(error);
      alert(error);
    }
  }

  async onDeleteNews(postId: string, index: number) {
    try {
      await this.updatePost(postId, { status: 'disabled' });
      let allNews = this.state.allNews;
      allNews.splice(index, 1);
      this.setState({ allNews: allNews });
    } catch (error) {
      alert(error);
    }
  }

  //////////////////RESOURCE FUNCTIONS///////////
  async refreshData() {
    let response = await this.getPost(
      this.state.pagination.offset,
      this.state.pagination.limit,
      this.state.searchText
    );
    let pagination = {
      ...this.state.pagination,
      total: response.meta.total,
      count: response.meta.count,
    };
    this.setState({
      allNews: response.data,
      pagination: pagination,
      totalNews: response.meta.totalNews,
    });
  }
  async loadMore() {
    let newOffset = this.state.pagination.offset + this.state.pagination.count;
    let response = await this.getPost(
      newOffset,
      this.state.pagination.limit,
      this.state.searchText
    );
    let pagination = {
      ...this.state.pagination,
      total: response.meta.total,
      offset: newOffset,
      count: response.meta.count,
    };
    this.setState({
      allNews: this.state.allNews.concat(response.data),
      pagination: pagination,
      totalNews: response.meta.totalNews,
    });
  }
  onSearch(value: string) {
    this.setState(
      {
        searchText: value,
        pagination: { ...this.state.pagination, offset: 0 },
      },
      async () => {
        this.refreshData();
      }
    );
  }
  async getPost(offset = 1, limit = 10, text = ''): Promise<any> {
    let { data } = await axios.get(SERVER_URL + '/post', {
      params: {
        offset,
        limit,
        text,
      },
    });
    return data;
  }
  async updatePost(postId: string, body: any): Promise<any> {
    let { data } = await axios.patch(SERVER_URL + `/post/${postId}`, body);
    return data;
  }

  //////////////////////////////////////////////

  render(): JSX.Element {
    let loadMore: any = (
      <div onClick={() => this.loadMore()} className='loadMore'>
        <button>Load more...</button>
      </div>
    );
    if (
      this.state.pagination.offset + this.state.pagination.count >=
      this.state.pagination.total
    ) {
      loadMore = null;
    }
    return (
      <div data-testid="app-root" className='App'>
        <Header
          totalNews={this.state.totalNews}
          search={this.onSearch.bind(this)}
        />
        <br />
        <div className='container'>
          {this.state.allNews.map((news, index) => {
            return (
              <News
                news={news}
                deleteNews={(e: any) => this.onDeleteNews(e, index)}
                key={news._id}
              ></News>
            );
          })}
          {loadMore}
          {!this.state.allNews.length ? (
            <h2 style={{ textAlign: 'center' }}>
              We do not have items with this query:{' '}
              <span style={{ fontStyle: 'italic' }}>
                {this.state.searchText}
              </span>
            </h2>
          ) : null}
        </div>
      </div>
    );
  }
}
export default App;
