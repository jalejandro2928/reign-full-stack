import React from 'react';
import { render, screen, cleanup } from '@testing-library/react';
import App from './App';
import Header from '../../components/Header/Header';
import News from '../../components/News/News';
import renderer from 'react-test-renderer';
afterEach(() => {
  cleanup();
});

test('Should render App component', () => {
  let AppComponent = <App />;
  render(AppComponent);
  let appElement = screen.getByTestId('app-root');
  expect(appElement).toBeInTheDocument();
});

describe('Testing child components', () => {
  test('Should render Header component', () => {
    render(<Header totalNews={200} search={() => {}} />);
    let headerElement = screen.getByTestId('app-header-test');
    expect(headerElement).toBeInTheDocument();
  });
  test('Should render News component', () => {
    let news = {
      _id: 'adsasarefaerfad',
      author: 'Jose Alejandro Concepcion',
      story_title: 'Tell me about NodeJs + Typescript',
      story_url: 'https://josealejandro2928.github.io/portfolio/',
      created_at: '2021-06-03T15:42:40.000Z',
    };
    let newsRenderInstance = renderer.create(
      <News news={news} deleteNews={() => {}} />
    );
    render(<News news={news} deleteNews={() => {}} />);
    let newsElement = screen.getByTestId(`news_${news._id}`);
    expect(newsElement).toBeInTheDocument();
    let meta: any = newsRenderInstance.toJSON();
    expect(meta.props).toEqual({
      'data-testid': 'news_adsasarefaerfad',
      className: 'News',
    });
  });
});
