import syncPost from '../controller/sync';

const TIME_TASK = 1000 * 60 * 60;
const taskToSyncTheArticlesDataBases = async (initial = false) => {
  try {
    if (initial) {
      await syncPost(10); ///Sync the last 200 articles;
    }
    setInterval(async () => {
      try {
        console.log('RUNING THE TASK...');
        await syncPost(10); ///Sync the last 200 articles;
      } catch (error) {
        console.log('ERROR FATAL TO GET THE TASK OF SYNC...', error.message);
      }
    }, TIME_TASK);
  } catch (error) {
    console.log('ERROR FATAL TO GET THE TASK OF SYNC...', error.message);
  }
};

export default taskToSyncTheArticlesDataBases;
