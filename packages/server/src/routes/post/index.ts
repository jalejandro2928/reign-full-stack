import { Express, Request, Response } from 'express';
import listPost from './controller/list';
import syncPost from './controller/sync';
import updatePost from './controller/update';

export const register = (app: Express) => {
  const path: string = '/post';

  app.get(path, async (req: Request, res: Response) => {
    try {
      const query: any = req.query;
      const response = await listPost(query);
      return res.status(200).json(response);
    } catch (error) {
      return res.status(error.status || 500).json({ message: error.message });
    }
  });

  app.get(path + '/sync', async (req: Request, res: Response) => {
    try {
      const totalPages = +(req.query.totalPages || 1);
      const response = await syncPost(totalPages);
      const data = {
        data: response,
        meta: {
          total: response.length,
          totalPages,
        },
      };
      return res.status(200).json(data);
    } catch (error) {
      return res.status(error.status || 500).json({ message: error.message });
    }
  });

  app.patch(path + '/:postId', async (req: Request, res: Response) => {
    try {
      console.log(req.params.postId);
      const response: any = await updatePost(req.body, req.params.postId);
      return res.status(201).json(response);
    } catch (error) {
      return res.status(error.status || 500).json({ message: error.message });
    }
  });
};
