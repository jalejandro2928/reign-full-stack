import models from '../../../models/index';
import axios from 'axios';
import * as bluebird from 'bluebird';

const EXTERNAL_URL =
  'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
const syncPost = async (totalPage = 50) => {
  try {
    let page = 0;
    console.log(
      'Fetching from: https://hn.algolia.com/api/v1/search_by_date?query=nodejs all pages'
    );
    let allPromises: any[] = [];
    for (let i = 0; i < totalPage; i++) {
      allPromises.push(i);
    }
    console.log(
      '🚀 ~ file: sync.ts ~ line 14 ~ syncPost ~ allPromises',
      allPromises
    );
    let articles: any[] = [];
    console.log('FETCHING ALL ARTICLES');
    allPromises = await Promise.all(
      allPromises.map(async (page) => {
        await delayMs(50);
        return axios.get(EXTERNAL_URL, {
          params: {
            page: page,
          },
        });
      })
    );
    console.log('GOT ALL ARTICLES');

    allPromises.map((dataResolved) => {
      articles = articles.concat(dataResolved.data.hits);
    });
    console.log(`total: ${articles.length}, pages: ${totalPage}`);
    ////////////CREATING MY ARTICLES //////////
    let createPosts: any[] = [];
    await bluebird.Promise.mapSeries(articles, async (articleX, index) => {
      let post = await models.Post.findOne({ objectID: articleX.objectID });
      if (!post) {
        post = new models.Post({ ...articleX });
        post = await post.save();
        createPosts.push(post);
      }
    });
    //////////////////////////////////////////
    console.log('******Finish the sync correctly*******');
    return createPosts;
  } catch (error) {
    console.log('Error fatal en el proceso de tomar las cosas', error.message);
    throw error;
  }
};

async function delayMs(delay = 200): Promise<any> {
  return new Promise((resolve, _) => {
    setTimeout(() => {
      return resolve(true);
    }, delay);
  });
}

export default syncPost;
