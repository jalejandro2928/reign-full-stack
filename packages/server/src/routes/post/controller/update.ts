import models from '../../../models/index';

const updatePost = async (body: any, postId: string) => {
  let updateBody: any = {};

  ['status', 'title', 'story_url', 'url', 'author', 'story_title'].map((x) => {
    if (body.hasOwnProperty(x)) {
      updateBody[x] = body[x];
    }
  });
  console.log(updateBody);
  let response: any = {
    data: {},
  };
  let post = await models.Post.findById(postId || '-1');
  if (!post) {
    throw { status: 404, message: `The resource you find does not exist ` };
  }
  await post.update(updateBody);
  response.data = await models.Post.findById(post._id);
  return response;
};

export default updatePost;
