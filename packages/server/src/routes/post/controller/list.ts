import { IQuery } from './../../../models/post.model';
import models from '../../../models/index';

const listPost = async (query: IQuery) => {
  query.limit = query.limit ? +query.limit : 10;
  query.offset = query.offset ? +query.offset : 0;
  query.text = query.text ? query.text : '';
  let response: any = {
    data: [],
    meta: {
      total: 0,
      count: 0,
    },
  };
  let paramsQuery: any = {
    status: 'enabled',
  };
  if (query.text) {
    paramsQuery['$or'] = [
      {
        title: { $regex: query.text, $options: 'i' },
      },
      {
        story_title: { $regex: query.text, $options: 'i' },
      },
      {
        author: { $regex: query.text, $options: 'i' },
      },
    ];
  }

  let post = await models.Post.find(paramsQuery)
    .skip(query.offset)
    .limit(query.limit)
    .sort({ created_at: -1 });
  response.data = post;
  response.meta.count = post.length;
  let [totalQuery, fullTotal] = await Promise.all([
    models.Post.countDocuments(paramsQuery),
    models.Post.countDocuments(),
  ]);
  response.meta.total = totalQuery;
  response.meta.totalNews = fullTotal;
  return response;
};

export default listPost;
