import express, { Express, Request, Response } from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import dotenv from 'dotenv';
import cors from 'cors';
import morgan from 'morgan';
//////////// IMPORTING MONGO SETUP ////////
import { connectDb } from './models/index';
/////////// IMPORTING THE ROUTES ///////
import * as postRoutes from './routes/post/index';
import taskToSyncTheArticlesDataBases from './routes/post/tasks/index.task';

// initialize configuration
dotenv.config();
const port = process.env.SERVER_PORT;

const app: Express = express();
app.use(cors());
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
///// CONFIGURATION OF MORGAN //////////
app.use(morgan('dev'));

app.get('/', (req: Request, res: Response) => {
  res.send('Hello world');
});

//////// Configure routes ///////////////////////////
postRoutes.register(app);
////////////////////////////////////////////////

connectDb()
  .then(async () => {
    // start the Express server
    app.listen(port, async () => {
      console.log(`Server started at http://localhost:${port}`);
      console.log(`Node environment is: ${process.env.NODE_ENV}`);
      /////////RUNING TASKS////////////////
      const time = setTimeout(() => {
        taskToSyncTheArticlesDataBases(true);
        clearTimeout(time);
      }, 5000);
      ////////////////////////////////////
      //////////// LOGGING ///////////
      // mongoose.set('debug', (collectionName: any, method: any, query: any, doc: any) => {
      //   console.log(`${collectionName}.${method}`, JSON.stringify(query), doc);
      // });
    });
  })
  .catch((error) => {
    console.log(`CONEXION A MONGODB: ${error}`);
  });
