import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const postSchema = new mongoose.Schema(
  {
    objectID: {
      type: String,
      unique: true,
    },
    story_title: {
      type: String,
    },
    title: {
      type: String,
    },
    story_id: {
      type: Number,
    },
    created_at: {
      type: Date,
      required: true,
    },
    story_url: {
      type: String,
    },
    parent_id: {
      type: Number,
    },
    url: {
      type: String,
    },
    author: {
      type: String,
    },
    story_text: {
      type: String,
    },
    comment_text: {
      type: String,
    },
    status: {
      type: String,
      enum: ['enabled', 'disabled'],
      default: 'enabled',
    },
  },
  { timestamps: true }
);

const Post = mongoose.model('Post', postSchema);

export default Post;

export interface IQuery {
  limit: number;
  offset: number;
  text: string;
}
