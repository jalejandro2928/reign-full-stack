import mongoose from 'mongoose';
import Post from './post.model';

const connectDb = () => {
  console.log('🚀 DATABASE_URL:', process.env.DATABASE_URL);
  return mongoose.connect(process.env.DATABASE_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
};

const models = { Post };

export { connectDb };

export default models;
