## Full Stack Application React and Nodejs

A complete end to end solution

### Client

Client was made using **React**, in this application you can list the must recent post, and you can search by author or title.
To improve performance a pagination feature is implemented.

1. The client project is in this path **'./packages/web'**.
2. In order to run the client whithout docker feature you must have node and yarn
3. Runinng this command: **yarn web-dev** and open in the browser <http://localhost:3000>
4. The server app must be running to be able to  list the posts

![Ejemplo de imagen](http://react-movies-catalog.surge.sh/monorepo.jpeg)

### Server

The application server was made using **NodeJs** , **ExpressJs** and **TypeScript**, as database it uses **MongoDb**.
The application is simple, it only has a Post model to model the articles.
The system has a task that runs every 1 hour to retrieve the news from <https://hn.algolia.com/api/v1/search_by_date?query=nodejs> and synchronize them with our DB.
When starting the server the system initially fetch the news to load the system from a startup.
1. The server project is in this path **'./packages/server'**.
2. In order to run the server whithout docker feature you must have node, yarn and mongodb installed and configurated
3. Runinng this command: **yarn server-dev** and you can make a get request in  <http://localhost:3333/post>

# With Docker
You must have docker to be able to run the container for all images.
All the commands below must be run from the repository root folder.
## Build the react app
1. **docker build -t "react-app" ./packages/web/**

## Build the api server
2. **docker build -t "api-server" ./packages/server/**

## Run your dockers containers, enyoy your mern app
3. **docker-compose up**